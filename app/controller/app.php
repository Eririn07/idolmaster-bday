<?php
Namespace Controller;

class App {
    public function get_hello($f3){
        $f3->set('today',date("d-m-Y"));
        $today=date("m-d");
//        echo $today;
        $sql="SELECT * FROM birthday WHERE birth LIKE '%$today'";
        if ($row=$f3->get("DB")->exec($sql)) {
            $f3->set("data.birthday",$row);
        }

        \View\Template::render("index.html", "iDOLM@STER Birthday Reminder");
    }

    public function get_tomorrow($f3){
        $tomorrow=date('Y-m-d', strtotime(' +1 day'));
        $f3->set('tomorrow',$tomorrow);
        $tmr=date('m-d', strtotime(' +1 day'));
        $sql="SELECT * FROM birthday WHERE birth LIKE '%$tmr'";
        if ($row=$f3->get("DB")->exec($sql)) {
            $f3->set("data.birthday",$row);
        }

        \View\Template::render("tomorrow.html", "iDOLM@STER Birthday Reminder");
    }

    public function get_month($f3){
        $month=date('n');
//        echo $month;
        $f3->set('month',date('M'));
        $sql="SELECT * FROM birthday WHERE MONTH(birth) = $month";
//        echo $sql;
        if ($row=$f3->get("DB")->exec($sql)) {
            $f3->set("data.birthday",$row);
        }

        \View\Template::render("month.html", "iDOLM@STER Birthday Reminder");
    }

    public function get_nmonth($f3){
        $month=date('n', strtotime('+1 month'));
//        echo $month;
        $f3->set('nmonth',date('M', strtotime('+1 month')));
        $sql="SELECT * FROM birthday WHERE MONTH(birth) = $month";
//        echo $sql;
        if ($row=$f3->get("DB")->exec($sql)) {
            $f3->set("data.birthday",$row);
        }

        \View\Template::render("nmonth.html", "iDOLM@STER Birthday Reminder");
    }

    public function get_input($f3){
        \View\Template::render("input.html", "iDOLM@STER Birthday Reminder");
    }

    public function post_input($f3){
        $name = $_POST['name'];
        $date = $_POST['datepicker'];
        $pict = $_POST['pict'];
        $dt = date("Y-m-d",strtotime($date));

        $sql = "INSERT INTO birthday (name, birth, picture)
        VALUES ('$name', '$dt', '$pict')";

        //echo "<script>console.log('Debug Objects: " . $sql . "' );</script>";

        if ($f3->get("DB")->exec($sql)) {
            $f3->reroute("/idoldb");
        }
    }

    public function get_idoldb($f3){
        $sql="SELECT * FROM birthday";
        if ($row=$f3->get("DB")->exec($sql)) {
            $f3->set("data.birthday",$row);
        }

        \View\Template::render("idoldb.html", "iDOLM@STER Birthday Reminder");
    }

}